# Create image based off of the official Node 6 image
FROM node:10.15.1

# Create a directory where our app will be placed
RUN mkdir -p /usr/src/app

# Change directory so that our commands run inside this new dir
WORKDIR /usr/src/app

#EXPOSE THE PORT
#EXPOSE 5000

# Serve the app
CMD ["node", "server.js"]